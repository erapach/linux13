# -*- coding: utf-8 -*-


############################################################
# Class
############################################################

import sys
import random

class Person():
        pass

class Employee(Person):

    def __init__(self, name, surname):
        self.name = name
        self.surname = surname
        self.salary = random.randint(1000, 2000)

    def __str__(self):
        return self.name + " " + self.surname + " " +str(self.salary)

    def increase_salary(self, salary):
        self.salary += salary

    def decrease_salary(self, salary):
        self.salary -= salary

print sys.argv, "list of arguments"
#emp = Employee(sys.argv[1], sys.argv[2])
#print emp

#emp.increase_salary(1000)
#print emp


############################################################
# Iterator
############################################################

a = [1,2,3,4,5]

print type(a)
for elem in a:
    print elem

class Container():

    def __init__(self):
        self.con = [x for x in range(1,10)]
        self.counter = 0

    def __iter__(self):
        return self

    def next(self):
        if self.counter >= 5:
            raise StopIteration
        self.counter += 1
        return self.counter

con = Container()

for elem in con:
    print elem
    

############################################################
# Generatr
############################################################

seasons = ['Spring', 'Summer', 'Fall', 'Winter']

print list(enumerate(seasons,3))
"""
list1 = []
def enumerate_function(sequence, start = 0):
    tup = 0
    iterator = 0
    for elem in sequence:
        tup[0] = iterator
        tup[1] = elem
        iterator += 1
        list1.append(tup)

for i in list1:
    print i
        

print enumerate_function(seasons,3)

"""

def enumerate_function2(sequence, start = 0):
    n=start

    for elem in sequence:
        yield n,elem
        n += 1


gen = enumerate_function2(seasons,3)
print gen.next()
print gen.next()
print gen.next()

gen = enumerate_function2(seasons,3)
print list(gen)


a = ['ala']
print a*3
print a+a+a

a[:0] = a*2
print a

if not [0]:
    print "wwww"

t = 55, 99
print type(t)

a1 = range(3,8,2)
a2 = [ x for x in range(1,10) if x not in a1]
print a2

programers = frozenset(['Jack', 'Sam', 'Susan'])
menagers = set(['Jane', 'Jack', 'Susan', 'Zack'])
print programers - menagers

import re

a = 'aa'
#print re.match("*b+a{2}", a).group()
#print re.match("[a][a]", a).group()
#print re.match(".*b*a{2}", a).group()
print re.match("[aa]", a).group()




############################################################
# Wielodziediczenie
############################################################

class D(object):
    arg = 20

class C(D):
    arg = 30
    
class B(D):
    pass

class A(B,D):
    pass

a= A()
print a.arg

#2.7 wgłąb
#3.0 wszerz
















