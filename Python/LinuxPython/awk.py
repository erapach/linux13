import sys

f = sys.stdin
# If you need to open a file instead:
#f = open('your.file')
for line in f:
    fields = line.strip().split()
    # Array indices start at 0 unlike AWK
    print(fields[0])
