#!/usr/bin/env python
# -*- coding: utf-8 -*- 
# Program Python sprawdzajacy dwie
# wartosci czy sa unikalne 
# Funkcja sciagajaca wartosci unikatowe
def unique(lista):
 
    # Inicjuje pusta liste
    lista_uniq = []
     
    # Sprawdza wszystkie elementy
    for element in lista:
        # Sprawdza czy element znajduje sie w liscie uniq
        if element not in lista_uniq:
            lista_uniq.append(element)
    # Wypisuje liste
    for element in lista_uniq:
        print element,
     
   
 
# Dane
lista1 = [10, 20, 10, 30, 40, 40,10,50,50]
print("Unikatowe elementy pierwszej listy to:")
print ""
unique(lista1)
 
print ""

lista2 =[1, 2, 1, 1, 3, 4, 3, 3, 5]
print("\n Unikatowe elementy drugiej listy to:")
print ""
unique(lista2)
