﻿#comment

"""
comment

"""

############################################################
#Data type - dynamiczne typowanie
############################################################

a = 1
print type(a) # int
a = "1"
print type(a) # string
a = 1.0
print type(a) # float

############################################################
#Tuple - krotka
############################################################
a = (1,2,3,4)
print type(a)

print a[0]
print a[1]
print a[2]

    #tuple is not immutable
    #a[0] = 99

a = 1,2,3,4,5,6
print type(a)

a = tuple()
print type(a)

a = 1
b = 2
a,b = b,a
print a
print b


############################################################
#List
############################################################

a = [1,2,3,4]
print type(a)
print a[0]
print a[1]
print a[2]

a[0] = "a"
print a

a.append(9)
print a

#print dir(a)
a.insert(0, 7) # index value
print a

del a[0]
print a

a = range(1,10)
print a

for elem in a:
    print elem

#comprehenction list
#lepsze
a = [x *2 for x in range(1,10)]
print a

#gorsze
new = []
for elem in range(1,10):
    if elem > 3:
        new.append(elem * 2)
print new

if 1<5:
    pass
elif 1<10:
    print ""
else:
    print ""


############################################################
# Slices
############################################################

a = [1,2,3,4,5]
b = (1,2,3,4,5)
c = "ggdksdklskkhg"

print a[0:4]
print b[0:4:2]
print c[0:4]

#last element from list a
print a[-1]
print  a[len(a) - 1]
print a[-1:-4:-1] #ostatni param w przeciwnym kierunku
print c[::-1]

############################################################
# Set
############################################################

a = [1,2,12,1,2,111,2]
print set(a)

a = "samplllee"
setA = set(a)
for elem in setA:
    print elem

a = {1,1,2,2,3,4}
print a

a = set([1,1,2,3,(1,2)])
print a

a.add("aaa")
print a

a.remove("aaa") #value as argument
print a

print 1 in a

#########################Fałsze 0 [] () zwracają false

#frozenset is immutable
#structure spposite to common set
a = [1,1,2,2,3,4]
print frozenset(a)

############################################################
# Dictionary
############################################################

dic = {
    "a":1,
    "b":2,
    "c":3
    }
print dic
print dic["a"]
print dic["b"]

dic["d"] = 4
print dic

del dic["d"]
print dic

print dic.keys()
print dic.values()
print dic.items()

############################################################
# Syntax
############################################################

#help(str)

#konwersja
str(1)
int("1")

a = "sample"
print a.index("m")
a = ["a", "b", "c"]

print "---".join(a)

############################################################
# ex 1
############################################################

text = "She sells sea shells"
list = text.split()
result = " ".join(list[::-1])
print result

############################################################
# ex 2
############################################################

text = "eve has a cat"

numbers = {
    "abc": 2,
    "def":3,
    "ghi":4,
    "jkl":5,
    "mno":6,
    "prs":7,
    "tuv":8,
    "wxz":9,
    " ": "#"
    }

result = ""

#inputText = raw_input("Enter text: ")
#print inputText

for letter in text.lower():
    for key in numbers:
        if letter in key:
            result += (key.index(letter)+1) * str(numbers[key])
print result

############################################################
# ex 3
############################################################

num = 177
print len(set(str(num)))

############################################################
# ex 4
############################################################

listAll = []

for x in range(1,7):
    for y in range(1,7):
        listAll.append((x,y))

listA = []
listB = []
listC = []

for tuple in listAll:
    if tuple[0] == tuple[1]:
        listA.append(tuple)
    elif tuple[0] < tuple[1]:
        listB.append(tuple)
    else:
        listC.append(tuple)

print listA
print listB
print listC

resultA = 0
resultB = 0
resultC = 0

for item in range(len(listA)):
    temp = listA.pop()
    resultA += temp[0] + temp[1]

for item in range(len(listB)):
    temp = listB.pop()
    resultB += temp[0] + temp[1]

for item in range(len(listC)):
    temp = listC.pop()
    resultC += temp[0] + temp[1]

print "resultA = " + str(resultA)
print "resultB = " + str(resultB)
print "resultC = " + str(resultC)


############################################################
# ex 5
############################################################

name1 = "A toyota".lower().replace(" ", "")
isPali = True

for i in range(len(name1)/2):
    if name1[i] == name1[-i-1]:
        continue
    else:
        isPali = False
        break
    
print isPali
    

print 2**38
s = "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj"
list = []
for i in s:
    list.append(i)

list2 =[]

for i in range(len(s)-3):
    if list[i] == " ":
        list2.append(" ")
    else:
        list2.append(chr(ord(list[i])+2))
print "".join(list2)







































































